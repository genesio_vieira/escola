<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Turma;


class TurmaController extends Controller
{
    private $turma;

    public function __construct(Turma $turma)
    {
        $this->turma = $turma;
    }
    public function index()
    {
        $turmas=$this->turma->all();
        return view('admin.turma.index', compact('turmas'));
    }


    public function create()
    {
        $title ="Cadastro de Turmas";
        //return view('admin.turma.create-edit');
        return view('admin.turma.create-edit',compact('turma','title'));
    }

    public function store(Request $request)
    {

        //pega todos os dados
        $dataForm=$request->all();
        //faz cadastro
        $insert=$this->turma->create($dataForm);

            if($insert)
                return redirect('admin/turma/');

            else
                return redirect() ->back();
    }

    public function show($id)
    {
      $turma=$this->turma->find($id);
      $title ="Excluir turma?";
      return view('admin.turma.delete',compact('turma','title'));
    }

    public function edit($id)
    {
        $turma=$this->turma->find($id);
        $title ="Editar Turma";

        return view('admin.turma.create-edit',compact('turma','title'));
    }

    public function update(Request $request, $id)
    {
       $dataForm=$request->all();
       $turma=$this->turma->find($id);
       $update=$turma->update($dataForm);

       if($update)
       return redirect('admin/turma/');
       else
       return redirect() ->back();
    }


    public function destroy($id)
    {
        $turma=$this->turma->find($id)->delete();
        return redirect('admin/turma/');
    }
}
