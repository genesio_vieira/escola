<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Professor;

class ProfessorController extends Controller
{
    private $professor;

    public function __construct(Professor $professor)
    {
        $this->professor = $professor;
    }
    public function index()
    {
        $professors=$this->professor->all(); // retorna todos o objeto aluno do banco
        return view('admin.professor.index', compact('professors'));

    }


    public function create()
    {
      $title ="Cadastro de  Professor(a)";

      return view('admin.professor.create-edit',compact('professor','title'));
    }


    public function store(Request $request)
    {
          //pega todos os dados
        $dataForm=$request->all();
        //faz cadastro
        $insert=$this->professor->create($dataForm);

            if($insert)
                return redirect('admin/professor');

            else
                return redirect() ->back();
    }


    public function show($id)
    {
      $professor=$this->professor->find($id);
      $title="Excluir Professor(a)?";
      return view ('admin.professor.delete',compact('professor','title'));
    }


    public function edit($id)
    {
      $professor=$this->professor->find($id);
      $title ="Editar Professor(a)";

      return view('admin.professor.create-edit',compact('professor','title'));
    }


    public function update(Request $request, $id)
    {
      $dataForm=$request->all();
      $professor=$this->professor->find($id);
      $update=$professor->update($dataForm);
      if($update)

      return redirect('admin/professor');
      else
      return redirect() ->back();
    }


    public function destroy($id)
    {
      $professor=$this->professor->find($id)->delete();
      return redirect('admin/professor');
    }
}
