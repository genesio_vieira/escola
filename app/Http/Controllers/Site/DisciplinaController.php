<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Disciplina;

class DisciplinaController extends Controller
{
    private $diciplina;

    public function __construct(Disciplina $disciplina)
    {
        $this->disciplina = $disciplina;
    }
    public function index()
    {
        $disciplinas=$this->disciplina->all();
        return view('admin.disciplina.index', compact('disciplinas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $title ="Cadastro de  Disciplina";

      return view('admin.disciplina.create-edit',compact('disciplina','title'));
    }

    public function store(Request $request)
    {
        //pega todos os dados
        $dataForm=$request->all();
        //faz cadastro
        $insert=$this->disciplina->create($dataForm);

            if($insert)
                return redirect('admin/disciplina/index');

            else
                return redirect() ->back();
    }


    public function show($id)
    {
      $disciplina=$this->disciplina->find($id);
      $title="Excluir Disciplina";
      return view ('admin.disciplina.delete',compact('disciplina','title'));
    }


    public function edit($id)
    {
      $disciplina=$this->disciplina->find($id);
      $title ="Editar Disciplina?";

      return view('admin.disciplina.create-edit',compact('disciplina','title'));
    }


    public function update(Request $request, $id)
    {
      $dataForm=$request->all();
      $disciplina=$this->disciplina->find($id);
      $update=$disciplina->update($dataForm);

      if($update)

      return redirect('admin/disciplina/index');
      else
      return redirect() ->back();
    }


    public function destroy($id)
    {
        $disciplina=$this->disciplina->find($id)->delete();
        return redirect('admin/disciplina/index');
    }
}
