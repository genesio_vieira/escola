<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Aluno;
use App\Model\Turma;

class AlunoController extends Controller
{
    private $aluno;
    private $turma;

    public function __construct(Aluno $aluno, Turma $turma)
    {
        $this->aluno = $aluno;
        $this->turma = $turma;

    }
    public function index()
    {
        $alunos=$this->aluno->all(); // retorna todos o objeto aluno do banco
        return view('admin.aluno.index', compact('alunos'));
    }


    public function create()
    {
      $title='Cadastro de Aluno';
    $alunos=$this->aluno->all();
    $turmas=$this->turma->pluck('nome','id')->all();
    //dd($turmas);

      //  return view('admin.aluno.create', compact('alunos','turma'));
        return view('admin.aluno.create',compact('alunos','turmas','title'));
    }

    public function store(Request $request)
    {
        //pega todos os dados
         $dataForm=$request->all();
        //faz cadastro
         $insert=$this->aluno->create($dataForm);

             if($insert)
                return redirect('admin/aluno/');

            else
                return redirect() ->back();

  }

     public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
