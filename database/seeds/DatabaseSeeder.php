<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //DB::statement('SET foreign_key_checks =0'); desativa chave estrangeira
         $this->call(UsersTableSeeder::class);
        //DB::statement('SET foreign_key_checks =1'); ativa chave estrangeira
    }
}
