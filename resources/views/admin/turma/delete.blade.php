@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')
<section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
          <h3 class="box-title">{{$title}}</h3>
          </div>

  <form action="{{url("admin/turma/delete/$turma->id")}}" method="POST">
      <input type="hidden" name="_method" value="DELETE">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="text" class="form-control" name='nome'  id="nome"  disabled="" value="{{$turma->nome}}">
<div class="box-body">
    <div class="form-group">
</div>
<div class="box-footer">
<button type="reset"class="btn btn-danger">Cancelar</button>
<button type="submit" class="btn btn-primary">Excluir</button>
</div>
</form>
</div>
<!-- /.box-body -->
</div></section>
@stop
