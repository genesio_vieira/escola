@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')

    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
              <h3 class="box-title">{{$title}}</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->


                @if(isset($turma))
                <form action="{{url("admin/turma/update/$turma->id")}}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                    <input type="text" class="form-control" name='nome'  id="nome" placeholder="Digite o nome da Turma" value="{{$turma->nome}}">

                    @else
                    <form role="form" method="post" action="{{url('admin/turma/store')}}" >
                     {!!csrf_field()!!}
                     <input type="" class="form-control" name='nome'  id="nome" placeholder="Digite o nome da Turma" value="{{ old('name')}}">
                  
                     @endif

                    <div class="box-body">
                        <div class="form-group">
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>


                </div>
                <!-- /.box-body -->



  </div></section>
@stop
