@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')

    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">{{$title}}</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              @if(isset($professor))
              <form action="{{url("admin/professor/update/$professor->id")}}" method="POST">
                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <label for="text">Nome do Professor(a)</label>
                  <input type="text" class="form-control" name= 'nome' id="nome" placeholder="Digite o nome da Disciplina" value="{{$professor->nome}}">
                  <label for="">FORMAÇÃO</label>
                  <input type="text" class="form-control" id="" name='formacao' placeholder="Digite o nome do Professor(a)" value="{{$professor->formacao}}">
                  <div class="box-footer">
                  @else
              <form role="form" method="post" action="{{url('admin/professor/store')}}">
                {!!csrf_field()!!}
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Nome do Professor(a)</label>
                    <input type="text" class="form-control" id="" name='nome' placeholder="Digite o nome do Professor(a)">
                  </div>
                  <label for="">FORMAÇÃO</label>
                  <input type="text" class="form-control" id="" name='formacao' placeholder="Digite o nome do Professor(a)">
                  <div class="box-footer">
                    @endif
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->



  </div></section>
@stop
