@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')

    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Cadastro de Professor(a)</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{url('admin/professor/store')}}">
                {!!csrf_field()!!}
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Nome do Professor(a)</label>
                    <input type="text" class="form-control" id="" name='nome' placeholder="Digite o nome do Professor(a)">
                  </div>
                  <label for="">FORMAÇÃO</label>
                  <input type="text" class="form-control" id="" name='formacao' placeholder="Digite o nome do Professor(a)">
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->



  </div></section>
@stop
