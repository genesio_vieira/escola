@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')

    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Cadastro de Alunos</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{url('admin/aluno/inserir')}}">
                {!!csrf_field()!!}
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Nome</label>
                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite o Nome do Aluno">
                  </div>
                  <div class="form-group">
                    <label for="">CPF</label>
                    <input type="text" name="cpf" class="form-control" id="cpf" placeholder="Digite o CPF do Aluno">
                  </div>
                  <div class="form-group">
                    <label>Escolha a Turma</label>
                    <select class="form-control"   >
                      @ foreach ($turma as $turma)
                      <option>  {{$turma->id}}</option>
                      @endforeach

                    </select>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->



  </div></section>
@stop
