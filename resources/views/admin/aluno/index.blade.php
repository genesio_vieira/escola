@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop

@section('content')

 <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Alunos Cadastrados</h3>

        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">

            <input type="text" name="table_search" class="form-control pull-right" placeholder="Pesquisar">
              <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th>ID</th>
            <th>Nome</th>
            <th>CPF</th>
            <th width="100px" > AÇÕES</th>
            </tr>
            @foreach($alunos as $aluno)
            <tr>
              <td>{{$aluno->id}}</td>
              <td> {{$aluno->nome}}</td>
              <td>{{$aluno->cpf}}
                    <td>
                            <a href="">
                                <span class="btn btn-success"><i class="fa fa-fw fa-pencil"></i> </span>
                        </td>

                        <td>
                              <a href="">
                                  <span class="btn btn-danger"> <i class="fa fa-fw fa-close"></i></span>
                          </td>
                      </tr>
            </tr>

            @endforeach
          </tbody></table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
@stop
