@extends('adminlte::page')

@section('title', 'Sistema Escolar')

@section('content_header')

@stop

@section('content')

    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">{{$title}}</h3>
                <table>
                  <tr></tr>
                  <tr></tr>

                @foreach ($turmas as $turma)
              <td>  {{$turma->id}}</td>
              <td>  {{$turma->nome}}</td>
                @endforeach
              </table>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{url('admin/aluno/store')}}">
                {!!csrf_field()!!}
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Nome</label>
                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite o Nome do Aluno">
                  </div>
                  <div class="form-group">
                    <label for="">CPF</label>
                    <input type="text" name="cpf" class="form-control" id="cpf" placeholder="Digite o CPF do Aluno">
                  </div>
                  <div class="form-group">
                    <label>Escolha a Turma: </label>
                  <select >
                <option> </option>

                </select>
              </div>



                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->



  </div></section>
@stop
