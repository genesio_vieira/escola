<?php
Route::get('/', 'Site\SiteController@index');
Route::group(['namespace'=>'Site', 'middleware'=>['auth']], function(){
Route::post('/admin/aluno/store', 'AlunoController@store');
Route::get('/admin', 'AdminController@index');
Route::get('/admin/aluno', 'AlunoController@index');
Route::get('/admin/aluno/create', 'AlunoController@create');
});
Route::group(['namespace'=>'Site', 'middleware'=>['auth']], function(){
Route::get('/admin/turma', 'TurmaController@index');
Route::get('/admin/turma/create', 'TurmaController@create');
Route::post('/admin/turma/store', 'TurmaController@store');
Route::get('/admin/turma/{id}/edit', 'TurmaController@edit');
Route::put('/admin/turma/update/{id}', 'TurmaController@update');
Route::get('/admin/turma/show/{id}', 'TurmaController@show');
Route::delete('/admin/turma/delete/{id}', 'TurmaController@destroy');

});
Route::group(['namespace'=>'Site', 'prefix'=> '/admin/disciplina','middleware'=>['auth']], function(){
Route::get('/index', 'DisciplinaController@index');
Route::get('/create', 'DisciplinaController@create');
Route::post('/store', 'DisciplinaController@store');
Route::get('/{id}/edit', 'DisciplinaController@edit');
Route::put('/update/{id}', 'DisciplinaController@update');
Route::get('/show/{id}', 'DisciplinaController@show');
Route::delete('/delete/{id}', 'DisciplinaController@destroy');
});
Route::group(['namespace'=>'Site', 'middleware'=>['auth']], function(){
Route::get('/admin/professor', 'ProfessorController@index');
Route::get('/admin/professor/create', 'ProfessorController@create');
Route::post('/admin/professor/store', 'ProfessorController@store');
Route::get('/admin/professor/{id}/edit', 'ProfessorController@edit');
Route::put('/admin/professor/update/{id}', 'ProfessorController@update');
Route::get('/admin/professor/show/{id}', 'ProfessorController@show');
Route::delete('/admin/professor/delete/{id}', 'ProfessorController@destroy');
});

Auth::routes();
